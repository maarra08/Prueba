package es.giss.onboarding;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication

public class MsGitlabApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsGitlabApplication.class, args);
	}

}